'use strict';
import './gallery.css';
import template from './gallery.jade';

export default class Gallery {
  constructor(options) {
    this.elem = document.createElement('div');
    this.elem.innerHTML = template(options);
    document.body.appendChild(this.elem);
  }

  render() {
    let defaultImg = this.elem.querySelector('img');
    let largeImg = this.elem.querySelector('.largeImg');
    let currentImg = document.createElement('img');

    currentImg.style.height = '240px';
    currentImg.style.width = '320px';
    currentImg.src = defaultImg.src;
    largeImg.appendChild(currentImg);

    function handler(event) {
      let target = event.target;

      if (target.tagName != 'IMG') return;
      currentImg.src = target.src;
    }

    this.elem.addEventListener('click', handler, false);
  }

}
