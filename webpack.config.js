var webpack = require('webpack');

module.exports = {
  entry: './frontend/app.js',

  output: {
    path:       __dirname + '/public',  // FS-путь к статике
    publicPath: '/', // Web-путь к статике (CDN?)
    filename:   'app.js',
    library:    '[name]'
  },

  watch: process.env.NODE_ENV === 'development',

  devtool: "cheap-module-inline-source-map",

  module: {
    loaders: [{
      test: /\.js$/,
      include: __dirname + "/frontend",
      loader: 'babel?presets=es2015'
    },
      {
        test: /\.css$/,
        include: __dirname + "/frontend",
        loader: 'style!css'
      },
      {
        test: /\.jade$/,
        include: __dirname + "/frontend",
        loader: 'jade'
      }
    ]
  },

  plugins: []
};

if (process.env.NODE_ENV === 'production') {

  module.exports.plugins.push(
      new webpack.optimize.UglifyJsPlugin({
        compress: {
          warnings: false
        }
      })
  );

}

